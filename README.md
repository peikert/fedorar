
<!-- README.md is generated from README.Rmd. Please edit that file -->

# fedorar

This is meant to debug semtree on fedora. The container has R, and the
packages devtools and semtree installed.

Build a container yourself with:

``` bash
make
```

Or pull the build one from:

``` bash
docker pull registry.git.mpib-berlin.mpg.de/peikert/fedorar
```

Start an interactive R session in the container with:

``` bash
docker run -it --rm fedorar R
```

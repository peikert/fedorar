---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

# fedorar

This is meant to debug semtree on fedora. The container has R, and the packages devtools and semtree installed.

Build a container yourself with:

```{bash, eval=FALSE}
make
```

Or pull the build one from:

```{bash, eval=FALSE}
docker pull registry.git.mpib-berlin.mpg.de/peikert/fedorar
```

Start an interactive R session in the container with:

```{bash, eval=FALSE}
docker run -it --rm fedorar R
```


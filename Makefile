PROJECT := fedorar
all: build
docker: build
build: Dockerfile
	docker build -t $(PROJECT) .

FROM fedora:34
RUN yum -y install R
RUN Rscript -e "install.packages('devtools', repos = 'cloud.r-project.org')" && \
  Rscript -e "devtools::install_github('brandmaier/semtree')"
